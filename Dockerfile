FROM php:7.2-fpm-alpine3.8

# Install dependencies
# RUN apk update && apk add --no-cache \
# 	build-essential \
# 	default-mysql-client \
# 	libpng-dev \
# 	libjpeg62-turbo-dev \
# 	libfreetype6-dev \
# 	locales \
# 	zip \
# 	jpegoptim optipng pngquant gifsicle \
# 	vim \
# 	unzip \
# 	curl

# Add Repositories
RUN rm -f /etc/apk/repositories &&\
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.8/main" >> /etc/apk/repositories && \
    echo "http://dl-cdn.alpinelinux.org/alpine/v3.8/community" >> /etc/apk/repositories

RUN apk update && apk add --no-cache \
	wget \
	php7-session \
	php7-json \
	php7-pdo \
	php7-openssl \
	php7-tokenizer \
	php7-pdo \
	php7-pdo_mysql \
	php7-xml \
	php7-simplexml \
	curl \
  build-base \
  libtool \
  autoconf \
  g++ \
	zlib

RUN apk update && apk add --no-cache \
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    libwebp

# Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN rm /var/cache/apk/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl
RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
